package com.ubtech.monty.checkstyle.rules;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.puppycrawl.tools.checkstyle.Checker;
import com.puppycrawl.tools.checkstyle.api.CheckstyleException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * JavaDoc first sentence period test.
 */
public class JavaDocPeriodTest {

    private EventCollector eventCollector;
    private Checker checker;

    @BeforeEach
    void beforeEach() throws CheckstyleException {
        eventCollector = new EventCollector();
        checker = Utils.createChecker(eventCollector);
    }

    @AfterEach
    void afterEach() {
        checker.destroy();
    }

    @Test
    public void success() throws CheckstyleException {

        final String file = "JavaDocPeriodMissing.java";
        final int errorCount = Utils.process(checker, file);
        assertThat(errorCount).isEqualTo(0);
        assertThat(eventCollector.getAuditEvents()).hasSize(0);
    }
}
